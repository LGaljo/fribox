if (!process.env.PORT) {
    process.env.PORT = 8080;
}

//vključimo razne knjižnice
var mime = require('mime');
//branje formov na spletni strani
var formidable = require('formidable');
var http = require('http');
//dostop do datotečnega sistema
var fs = require('fs-extra');
var util = require('util');
//pretvarjanje med absolutno in relativno potjo do datotek
var path = require('path');

//kje je naša podatkovna baza
var dataDir = "./data/";

//tukaj se začne veselje
//povemo kako naj naš strežnik deluje, ko pridemo na naslov
var streznik = http.createServer(function(zahteva, odgovor) {
   if (zahteva.url == '/') {
       posredujOsnovnoStran(odgovor);
       //ko pridemo na ./, posreduj osnovno stran
   } else if (zahteva.url == '/datoteke') { 
       posredujSeznamDatotek(odgovor);
       //dobimo sesznam datotek
   } else if (zahteva.url.startsWith('/brisi')) { 
       izbrisiDatoteko(odgovor, dataDir + zahteva.url.replace("/brisi", ""));
       //izbrišemo datoteko na serverju preko določenega naslova, ki je omenjen v naslovu
   } else if (zahteva.url.startsWith('/prenesi')) { 
       posredujStaticnoVsebino(odgovor, dataDir + zahteva.url.replace("/prenesi", ""), "application/octet-stream");
       //vrne nam statično datoteko
       //forcamo prenos statične vsebine
   } else if (zahteva.url == "/nalozi") {
       naloziDatoteko(zahteva, odgovor);
       //omogoča nam upload datoteke
   } else {
       posredujStaticnoVsebino(odgovor, './public' + zahteva.url, "");
       //če pa pride kakršnakoli druga zahteva, pa gremo v mapo public pogledati, če tam obstaja ta datoteka
   }
});

function posredujOsnovnoStran(odgovor) {
    posredujStaticnoVsebino(odgovor, './public/fribox.html', "");
}

function posredujStaticnoVsebino(odgovor, absolutnaPotDoDatoteke, mimeType) {
        fs.exists(absolutnaPotDoDatoteke, function(datotekaObstaja) {
            if (datotekaObstaja) {
                fs.readFile(absolutnaPotDoDatoteke, function(napaka, datotekaVsebina) {
                    if (napaka) {
                        //Posreduj napako
                    } else {
                        posredujDatoteko(odgovor, absolutnaPotDoDatoteke, datotekaVsebina, mimeType);
                    }
                })
            } else {
                //Posreduj napako
            }
        })
}

function posredujDatoteko(odgovor, datotekaPot, datotekaVsebina, mimeType) {
    if (mimeType == "") {
        odgovor.writeHead(200, {'Content-Type': mime.lookup(path.basename(datotekaPot))});    
    } else {
        odgovor.writeHead(200, {'Content-Type': mimeType});
    }
    
    odgovor.end(datotekaVsebina);
}

function posredujSeznamDatotek(odgovor) {
    //funkcija vrne seznam vseh datotek z njihovimi atributi
    odgovor.writeHead(200, {'Content-Type': 'application/json'});
    fs.readdir(dataDir, function(napaka, datoteke) {
        if (napaka) {
            //Posreduj napako
        } else {
            var rezultat = [];
            for (var i=0; i<datoteke.length; i++) {
                var datoteka = datoteke[i];
                var velikost = fs.statSync(dataDir+datoteka).size;    
                rezultat.push({datoteka: datoteka, velikost: velikost});
            }
            
            odgovor.write(JSON.stringify(rezultat));
            odgovor.end();      
        }
    })
}

function naloziDatoteko(zahteva, odgovor) {
    var form = new formidable.IncomingForm();
 
    form.parse(zahteva, function(napaka, polja, datoteke) {
        util.inspect({fields: polja, files: datoteke});
    });
 
    form.on('end', function(fields, files) {
        var zacasnaPot = this.openedFiles[0].path;
        var datoteka = this.openedFiles[0].name;
        fs.copy(zacasnaPot, dataDir + datoteka, function(napaka) {  
            if (napaka) {
                //Posreduj napako
            } else {
                posredujOsnovnoStran(odgovor);        
            }
        });
    });
}

streznik.listen(process.env.PORT, function() {
    console.log("Strežnik je pognan.");
})